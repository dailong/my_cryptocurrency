# MyCoin

- IT4371 My Cryptocurrency

# Môi trường phát triển

NodeJs

# Hướng dẫn cài đặt

chạy lệnh `yarn install` để cài đặt môi trường

# Hướng dẫn sử dụng

## Khởi tạo node


mặc định `yarn watch`

> sau khi chạy lệnh này thì hệ thống sẽ được khởi tạo tại địa chỉ `http://localhost:3001` là backend của hệ thống và `ws://localhost:5001` là địa chỉ mạng peer-to-peer để các node khác có thể join vào mạng

khởi tạo các node khác thì đổi 2 cổng `HTTP_PORT` và `P2P_PORT=5002`

sau đó xác định danh sách các node hàng xóm (node muốn kết nối) với `PEERS`

> Nếu có nhiều node, viết địa chỉ liên tiếp, cách nhau bởi dấu `,`

ví dụ:

`HTTP_PORT=3003 P2P_PORT=5003 PEERS=ws://localhost:5001,ws://localhost:5002 yarn watch`

câu lệnh trên tạo ra một node có địa chỉ là `http://localhost:3003` và `ws://localhost:5003`, kết nối với 2 node khác là `ws://localhost:5001` và `ws://localhost:5002`

## Tương tác với API

### tạo một transaction

Tạo `POST` request tới `localhost:HTTP_PORT/transaction`

với body như sau:

```
{
	"txType": String,
	"data": Number
	"recipient": {
		"publicKey": String,
		"balance": Number
	},
	"endTime": Number // only for smart contract
}
```

txType là `SMART_CONTRACT`, `TRANSFER`

> Với `SMART_CONTRACT`, trường data trên sẽ có dạng như sau: 

```
    {
		"senderAmount": Number,
		"recipientAmount": Number
	}
```

`senderAmount` và `recipientAmount` là số tiền gửi và tiền lãi

`endTime` là thời hạn kết thúc hợp đồng

`publicKey` là địa chỉ người nhận

### lấy thông tin ví

Tạo `GET` request tới `localhost:HTTP_PORT/wallet`

### lấy thông tin ví Miner

Tạo `GET` request tới `localhost:HTTP_PORT/miner`

### lấy thông tin Pool

Tạo `GET` request tới `localhost:HTTP_PORT/pool`

### Lấy thông tin Chain

Tạo `GET` request tới `localhost:HTTP_PORT/blocks`

### Mining

Tạo `GET` request tới `localhost:HTTP_PORT/mining`

## Tương tác với giao diện