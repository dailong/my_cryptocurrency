import { keyFromPublic } from '../utils/Crypto'
import { getBuffer } from '../utils'

class TransactionPool {
  constructor() {
    this.transactions = []
  }

  submit = transaction => {
    if (this.transactions.find(tx => tx.id === transaction.id)) {
      console.log('transaction existed!!')
      return false
    }

    if (!this.isValidTransaction(transaction)) {
      console.log('transaction is invalid')
      return false
    }

    this.transactions.push(transaction)
    return true
  }

  clear = () => {
    this.transactions = []
  }

  pull = () => {
    return this.transactions
  }

  isValidTransaction = transaction => {
    const { signature } = transaction
    const { publicKey } = transaction.sender
    let hash = transaction.hash()

    const key = keyFromPublic(publicKey)

    let isValid = key.verify(hash, getBuffer(signature))

    return isValid
  }
}

export default TransactionPool
