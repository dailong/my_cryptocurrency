import Block from './Block'

class BlockChain {
  constructor() {
    this.chain = [Block.genesis()]
  }

  lastBlock = () => {
    return this.chain[this.chain.length - 1]
  }

  addBlock = transactions => {
    const newBlock = Block.mineBlock(this.lastBlock(), transactions)

    if (newBlock) {
      // Change transaction state to CONFIRM
      newBlock.data.forEach(tx => tx.changeState())

      this.chain.push(newBlock)
      return newBlock
    }
  }

  isValidChain = chain => {
    // check the root of chain
    if (JSON.stringify(chain[0]) !== JSON.stringify(Block.genesis()))
      return false

    for (let i = 1; i < chain.length; i++) {
      const prevBlock = chain[i - 1]
      const block = chain[i]

      if (
        block.lastHashValue !== prevBlock.hashValue ||
        !Block.isValidBlock(block)
      ) {
        return false
      }
    }
    return true
  }

  replaceTheChain = newChain => {
    if (newChain.length <= this.chain.length) return
    if (!this.isValidChain(newChain)) return

    console.log('Replace new chain', newChain)
    this.chain = newChain
  }
}

export default BlockChain
