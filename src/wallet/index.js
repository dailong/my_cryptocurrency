import { genKeyPair } from '../utils/Crypto'
import { toHexString } from '../utils'
import Transaction, { TRANSACTION_TYPE } from '../transaction/Transaction'

class Wallet {
  constructor(INITIAL_BALANCE = 100) {
    this.balance = INITIAL_BALANCE
    this.keyPair = genKeyPair()
    this.publicKey = this.keyPair.getPublic().encode('hex')
  }

  getSmartContracts = chain => {
    let smartContracts = []

    for (let i = 1; i < chain.length; i++) {
      const block = chain[i]

      block.data.forEach(tx => {
        const { sender, content, recipient } = tx
        const { txType } = content

        if (txType !== TRANSACTION_TYPE.SMART_CONTRACT) return

        if (
          sender.publicKey === this.publicKey ||
          recipient.publicKey === this.publicKey
        ) {
          smartContracts.push(tx)
        }
      })
    }

    return smartContracts
  }

  availableBalance = chain => {
    let allowBalance = this.balance
    const smartContracts = this.getSmartContracts(chain)

    smartContracts.forEach(sc => {
      const {
        sender,
        recipient,
        content: { data }
      } = sc

      if (data.endTime < Date.now()) return

      if (sender.publicKey === this.publicKey) {
        allowBalance -= data.senderAmount
      } else if (recipient.publicKey === this.publicKey) {
        allowBalance -= data.recipientAmount
      }
    })

    return allowBalance
  }

  createTransaction = (content, recipient, chain) => {
    const { txType } = content
    switch (txType) {
      case TRANSACTION_TYPE.SMART_CONTRACT:
        const { senderAmount, recipientAmount } = content.data

        if (this.availableBalance(chain) < senderAmount) {
          console.log('Sender not have enough money to create transaction!')
          return
        }
        if (recipient.balance < recipientAmount) {
          console.log('Recipient not have enough money to create transaction!')
          return
        }
        break
      case TRANSACTION_TYPE.TRANSFER:
        const { data } = content

        if (this.availableBalance(chain) < data) {
          console.log('Not have enough money to create transaction!')
          return
        }
        break

      default:
        console.log('ERROR: Transaction type is unknown!')
    }

    const tx = new Transaction(
      { balance: this.balance, publicKey: this.publicKey },
      recipient,
      content
    )

    // sign transaction
    this.signTransaction(tx)

    return tx
  }

  signTransaction = transaction => {
    let hash = transaction.hash()

    const signature = this.keyPair.sign(hash).toDER()

    transaction.signature = toHexString(signature)
  }

  getBalance = chain => {
    let balance = 100
    for (let i = 1; i < chain.length; i++) {
      const block = chain[i]

      block.data.forEach(transaction => {
        const { sender, content, recipient } = transaction

        // console.log('=====TRANSACTION', transaction)

        if (content.txType !== TRANSACTION_TYPE.TRANSFER) {
          return balance
        }
        if (sender.publicKey === this.publicKey) {
          balance -= content.data
        } else if (recipient.publicKey == this.publicKey) {
          balance += content.data
        }
      })
      const { reward, minedBy } = block
      if (minedBy && minedBy === this.publicKey) {
        balance += reward
      }
    }
    this.balance = balance
    return balance
  }

  description = () => {
    return `
      Wallet ___
      balance: ${this.balance}
      publicKey: ${this.publicKey.toString()}
      `
  }
}

export default Wallet
